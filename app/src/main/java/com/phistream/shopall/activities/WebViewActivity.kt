package com.phistream.shopall.activities

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView

import com.phistream.shopall.R

class WebViewActivity : AppCompatActivity() {

    private var webView: WebView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.setContentView(R.layout.content_web_view)
        webView = findViewById(R.id.wvSites) as WebView
        webView!!.loadUrl(intent?.getStringExtra("url"))

    }
}


/** In JAVA
 *
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.phistream.shopall.R;

public class WebViewActivity extends Fragment {

private final static String ARG_SITE_NAME = "arg_site_name";
private WebView webView;

public WebViewActivity() {
}

public static WebViewActivity getNewInstance(String name) {
WebViewActivity fragment = new WebViewActivity();
Bundle bundle = new Bundle();
bundle.putString(ARG_SITE_NAME, name);
return fragment;
}

@Nullable
@Override
public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
return inflater.inflate(R.layout.content_web_view, container, false);
}

@Override
public void onActivityCreated(@Nullable Bundle savedInstanceState) {
super.onActivityCreated(savedInstanceState);
webView = (WebView) getView().findViewById(R.id.wvSites);
webView.loadUrl(getArguments().getString(ARG_SITE_NAME));
}
}
 */
